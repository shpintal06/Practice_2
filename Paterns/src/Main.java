import builder.Email;
import factory.Car;
import factory.CarFactory;
import factory.CarProvider;
import singleton.EmailService;

public class Main {

    public static void main(String[] args) {
        //Builder
        Email email = new Email.Builder("Dean", "Meeting")
                .withBody("Hello")
                .withPriority(10)
                .withAttachment()
                .build();
        Email email1 = new Email.Builder("Saske", "bla")
                .withAttachment()
                .build();


        print(email);
        print(email1);

        //Singleton
        EmailService.getInstance().sendMessage(email);


        //Factory
        Car sedan = CarFactory.getCar(CarProvider.SEDAN);
        sedan.beep();
        Car jeep = CarFactory.getCar(CarProvider.JEEP);
        jeep.beep();
    }

    public static void print(Email emailp) {
        System.out.println(emailp.getTo() + "\n" +
                emailp.getSubject() + "\n" +
                emailp.getBody() + "\n" +
                emailp.getPriority() + "\n" +
                emailp.attachment);
    }
}
