package singleton;

import builder.Email;

public class EmailService {

    private static EmailService emailService = new EmailService();

    private EmailService() {
    }

    public static EmailService getInstance() {
        return emailService;
    }

    public void sendMessage(Email email) {
        System.out.println("builder.Email has been sent to " + email.getTo() + " successfully.");
    }
}
