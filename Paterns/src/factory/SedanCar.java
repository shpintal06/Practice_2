package factory;

public class SedanCar implements Car {

    @Override
    public void drive() {
        System.out.println("Sedan drive");
    }

    @Override
    public void beep() {
        System.out.println("Sedan beep");
    }
}
