package factory;

public interface Car {
    void drive();
    void beep();
}
