package factory;

public class JeepCar implements Car {
    @Override
    public void drive() {
        System.out.println("Jeep drive");
    }

    @Override
    public void beep() {
        System.out.println("Jeep beep");
    }
}
