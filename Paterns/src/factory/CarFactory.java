package factory;

public class CarFactory {
    public static Car getCar(CarProvider carProvider) {
        switch (carProvider) {
            case JEEP:
                return new JeepCar();
            case MINIVAN:
                return new MinivanCar();
            case SEDAN:
                return new SedanCar();
            default:
                return new MinivanCar();
        }
    }
}
