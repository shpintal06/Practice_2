package factory;

public class MinivanCar implements Car {
    @Override
    public void drive() {
        System.out.println("Minivan drive");
    }

    @Override
    public void beep() {
        System.out.println("Minivan beep");
    }
}
