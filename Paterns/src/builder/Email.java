package builder;

public class Email {

    private String to;
    private String subject;

    private String cc;
    private String body;
    private int priority;
    public boolean attachment;

    public Email(Builder builder) {
        this.to = builder.to;
        this.subject = builder.subject;

        this.cc= builder.cc;
        this.body = builder.body;
        this.attachment = builder.attachment;
        this.priority = builder.priority;
    }

    public String getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public int getPriority() {
        return priority;
    }

    public String getBody() {
        return body;
    }

    public String getCc() {
        return cc;
    }

    public static class Builder {
        private String to;
        private String subject;

        private String cc;
        private String body;
        private int priority;
        private boolean attachment = false;

        public Builder(String to, String subject) {
            this.to = to;
            this.subject = subject;
        }


        public Builder withAttachment() {
            attachment = true;
            return this;
        }

        public Builder withCc(String cc) {
            this.cc = cc;
            return this;
        }

        public Builder withBody(String body) {
            this.body = body;
            return this;
        }

        public Builder withPriority(int priority) {
            this.priority = priority;
            return this;
        }

        public Email build() {
            return new Email(this);
        }
    }
}
